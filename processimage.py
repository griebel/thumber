"""@package ProcessImage
Documentation for this module.

More details.
"""
from os import mkdir, path
from PIL import Image
from PIL.ExifTags import TAGS

import resizeimage


class ProcessImage:
    """ Process images to create smaller ones and thumbnails.

    More details.
    """

    @staticmethod
    def is_jpeg(filename):
        """ Checks, if file ends with '.jpg'.
        @param filename The filename.
        """
        if filename.lower().endswith('.jpg'):
            return True
        else:
            return False

    @staticmethod
    def make_dir(dir_name):
        """ Create a new directory, if it doesn't exist.
        @param dir_name The directory name.
        """
        try:
            mkdir(dir_name)
        except OSError:
            pass

    @staticmethod
    def max_sizes(image, max_size, method=3):
        """ Find size for picture
        @param image The image file.
        @param max_size The maximal size in two dimensions.
        @param method The resize method.
        """
        im_aspect = float(image.size[0]) / float(image.size[1])
        out_aspect = float(max_size[0]) / float(max_size[1])

        if im_aspect >= out_aspect:
            return image.resize((max_size[0], int((float(max_size[0]) / im_aspect) + 0.5)), method)
        else:
            return image.resize((int((float(max_size[1]) * im_aspect) + 0.5), max_size[1]), method)

    @staticmethod
    def create_small(image, max_size, path):
        """ Generate a small image
        @param image The image file.
        @param max_size The maximal size in two dimensions.
        @param path The storage location.
        """
        img = image.copy()
        img.thumbnail(max_size, Image.ANTIALIAS)
        img.save(path, 'JPEG')

    @staticmethod
    def create_thumb(image, max_size, path):
        """ Generate a thumb image
        @param image The image file.
        @param max_size The maximal size in two dimensions.
        @param path The storage location.
        """
        img = image.copy()
        img = resizeimage.resize_cover(img, max_size)
        img.save(path, 'JPEG')

    def generate(self, source_dir, destination_dir, filename):
        """ Generate two JPEG files in format 1024x768 and 255x191.
        @param source_dir The source directory.
        @param destination_dir The destination directory.
        @param filename The filename.
        """
        self.make_dir(destination_dir + source_dir)
        small_img = destination_dir + source_dir + '/' + filename[:-4] + '-small.jpg'
        thumb_img = destination_dir + source_dir + '/' + filename[:-4] + '-thumb.jpg'

        if not path.isfile(small_img) or not path.isfile(thumb_img):
            file_path = path.join(source_dir, filename)
            img = Image.open(file_path)

            exif = img._getexif()
            if exif is not None:
                for self.tag, self.value in exif.items():
                    decoded = TAGS.get(self.tag, self.tag)
                    if decoded == 'Orientation':
                        if self.value == 3:
                            img = img.rotate(180, expand=True)
                        if self.value == 6:
                            img = img.rotate(270, expand=True)
                        if self.value == 8:
                            img = img.rotate(90, expand=True)
                        break

            self.create_small(img, (1024, 768), small_img)
            self.create_thumb(img, (255, 191), thumb_img)
