"""@package main
Documentation for this module.

More details.
"""
# global includes
from Tkinter import *
from threading import Thread
from os import walk

# local includes
from processimage import ProcessImage


class GUI:
    """"Class which includes all things done for the GUI."""

    def __init__(self):
        self.__root_window = Tk()
        self.__thread = None

    def start(self):
        """"Sets the GUI up and runs it."""
        self.__make_gui()
        self.__root_window.mainloop()

    def __make_gui(self):
        """"Function for creating the entire GUI."""
        entries = self.__make_form()
        self.__make_buttons(entries)

        self.__label_progress = Label(self.__root_window, width=20)
        self.__label_progress.pack(side=LEFT, padx=5, pady=5)
        self.__label_progress.configure(text="Status: Ready")

        self.__root_window.bind('<Return>', (lambda event, e=entries: self.__generate(e)))

    def __make_form(self):
        """"Method for creating the input fields."""
        entries = []

        field = 'Source'
        value = '../gallery'
        entry = self.__make_input(field, value)
        entries.append((field, entry))

        field = 'Destination'
        value = '../olis-corner/gallery'
        entry = self.__make_input(field, value)
        entries.append((field, entry))

        return entries

    def __make_input(self, field, value):
        """"Function to create a input field with a label before.
        @param field Name of text field.
        @param value Initial value of entry.
        """
        row = Frame(self.__root_window)
        lab = Label(row, width=15, text=field, anchor='w')
        ent = Entry(row)

        ent.insert(0, value)

        row.pack(side=TOP, fill=X, padx=5, pady=5)
        lab.pack(side=LEFT)
        ent.pack(side=RIGHT, expand=YES, fill=X)

        return ent

    def __make_buttons(self, entries):
        """"Creates the Button of the GUI.
        @param entries Text field and value.
        """
        self.__button_generate = Button(self.__root_window, text='Generate', width=8,
                                        command=(
                                            lambda e=entries: self.__generate(e)))
        self.__button_generate.pack(side=LEFT, padx=5, pady=5)
        button_quit = Button(self.__root_window, text='Quit', width=8, command=self.__quit)
        button_quit.pack(side=LEFT, padx=5, pady=5)

    def __generate(self, entries):
        """"Input values are fetched and image processing gets started.
        @param entries Input paths to create pictures from and to.
        """
        if self.__thread is None:
            text = ['', '']
            for counter, entry in enumerate(entries):
                field = entry[0]
                text[counter] = entry[1].get()
                print('%s: "%s"' % (field, text[counter]))
            self.__thread = Thread(target=self.__start_app, args=(text[0], text[1]))
            self.__thread.start()
            self.__button_generate.configure(text="Stop")
        else:
            self.__thread.do_run = False
            self.__thread.join()
            self.__thread = None
            self.__label_progress.configure(text="Status: Stopped")
            self.__button_generate.configure(text="Generate")

    def __quit(self):
        """"Input values are fetched and image processing gets started."""
        if self.__thread is not None:
            self.__thread.do_run = False
            self.__thread.join()
        self.__root_window.quit()

    @staticmethod
    def __get_progress(process, source_dir):
        """" Get the amount of pictures to work at. Makes it possible to show a total progress.
        @param process ProcessImage instance.
        @param source_dir Source directory.
        """
        counter = 0
        for root, sub_dirs, files in walk(source_dir):
            for filename in files:
                if process.is_jpeg(filename):
                    counter += 1

        return counter

    def __start_app(self, source, destination):
        """ Method to start using the application
        @param source Source path.
        @param destination Destination path.
        """
        done = False
        counter = 0
        source_dir = source + '/'
        destination_dir = destination + '/'
        process = ProcessImage()

        progress = self.__get_progress(process, source_dir)

        for root, sub_dirs, files in walk(source_dir):
            for filename in files:
                if process.is_jpeg(filename):
                    process.generate(root, destination_dir, filename)
                    counter += 1
                    self.__label_progress.configure(text="Progress: " + str(counter) + '/' + str(progress))
                    if not getattr(self.__thread, "do_run", True):
                        done = True
                        break
            if done:
                break

        self.__thread = None
        self.__label_progress.configure(text="Status: Done")
        self.__button_generate.configure(text="Generate")
